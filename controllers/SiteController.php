<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Libros;
use app\models\Autores;

class SiteController extends Controller
{
    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    
    public function actionIndex()
    {
        return $this->render('index');
        
    }
    
    public function actionConsulta1()
    {
        $datos=Libros::find()->all();
        return $this->render('libros',[
            'datos'=>$datos,
        ]);
    }
    
    public function actionConsulta2()
    {
        $datos=Autores::find()->all();
        return $this->render('autores',[
            'datos'=>$datos,
        ]);
    }
    public function actionEntrada()
    {
        return $this->render('entrada');
    }

    
}
