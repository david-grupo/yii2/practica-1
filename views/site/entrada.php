<?php
use yii\helpers\Html;
?>

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Mensaje de entrada</h3>
  </div>
  <div class="panel-body">
    Aplicacion realizada con Yii2 como ejercicio del curso de desarrollo de 
    aplicaciones con tecnología web. Alpe formacion, 2018.
    Consulta de base de datos 'titulos'.
    <?=Html::img("@web/imgs/biblioteca.jpg",[
             'class'=>'img-responsive',
         ]); ?>
  </div>
</div>
